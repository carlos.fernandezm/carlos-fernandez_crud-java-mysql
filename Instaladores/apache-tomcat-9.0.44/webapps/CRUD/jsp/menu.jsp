<%@page import="com.carlos.fernandez.util.Configuration"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="robots" content="noindex, nofollow">
<!-- Hojas de Estilos-->
<!-- bootstrap-4.2.1 CSS -->
<link rel="stylesheet" type="text/css"
	href="libraries/bootstrap-4.2.1/css/bootstrap.min.css">

</head>
<body>
	<nav class="navbar-expand-lg navbar navbar-light py-0 py-md-0" style="background-color: #6171ac;">
		<a class="navbar-brand " href="<%=Configuration.SRV_PRINCIPAL%>">
			Men&uacute; Principal </a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li><a class="nav-link" href="cliente" id="GRUPO">CLIENTE</a></li>
			</ul>
			<form class="form-inline" id="frmPrincipal" name="frmPrincipal"
				method="post" action="<%=Configuration.SRV_PRINCIPAL%>">
				<input type="hidden" id="action" name="action" value="logout">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item py-0"><a class="nav-link" href="#"
						onclick="document.frmPrincipal.submit()"><span
							class="fa fa-sign-out"></span>Salir</a></li>
				</ul>
			</form>

		</div>
	</nav>

	<!-- jquery-3.5.1 JS-->
	<script type="text/javascript" charset="UTF-8"
		src="libraries/jquery-3.5.1/jquery.min.js"></script>
	<!-- bootstrap-4.2.1 JS  -->
	<script type="text/javascript" charset="UTF-8"
		src="libraries/bootstrap-4.2.1/js/bootstrap.min.js"></script>
</body>
</html>