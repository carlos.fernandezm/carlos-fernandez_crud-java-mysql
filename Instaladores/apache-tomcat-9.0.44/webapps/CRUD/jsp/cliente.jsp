<%@page import="com.carlos.fernandez.util.Configuration"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="robots" content="noindex, nofollow">
<!-- Hojas de Estilos-->
<!-- bootstrap-4.2.1 CSS -->
<link rel="stylesheet" type="text/css"
	href="libraries/bootstrap-4.2.1/css/bootstrap.min.css">

<!-- bootstrap-table-1.15.5 CSS -->
<link rel="stylesheet" type="text/css"
	href="libraries/bootstrap-table-1.15.5/dist/extensions/group-by-v2/bootstrap-table-group-by.min.css">
<link rel="stylesheet" type="text/css"
	href="libraries/bootstrap-table-1.15.5/dist/bootstrap-table.min.css">
<title>CLIENTE</title>
</head>
<body>
	<div class="container">
		<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt"
			value="${csrfPreventionSalt}" />
		<jsp:include page="<%=Configuration.JSP_MENU%>"></jsp:include>
		<div class="row">
			<div class="col-12 text-center">

				<table id="tbCliente" data-toggle="table" data-toolbar="#toolbar"
					data-height="460" data-ajax="ajaxClientes">
					<thead>
						<tr>
							<th data-field="id" data-visible="false">ID</th>
							<th data-field="nombres">Nombres</th>
							<th data-field="apellidos">Apellidos</th>
							<th data-field="edad">Edad</th>
							<th data-field="editar" data-formatter="operateFormatterEditar"
								data-events="operateEventsEditar"></th>
							<th data-field="eliminar"
								data-formatter="operateFormatterEliminar"
								data-events="operateEventsEliminar"></th>
						</tr>
					</thead>
				</table>
				<button class="btn btn-sm btn-success" id="btnNuevo">Nuevo</button>
			</div>
		</div>
	</div>







	<div id="modVis" funcion="" class="modal fade" tabindex="-1"
		role="dialog" aria-labelledby="modVis" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<form role="form" action="#">
					<div class="modal-body pb-0">

						<div class="row mt-1">
							<input type="hidden" class="form-control form-control-sm"
								id="txtId" name="txtId" /> <label for="txtNombres"
								class="col-12 col-sm-3 col-form-label form-control-sm">Nombres:</label>
							<div class="col-12 col-sm-5">
								<input type="text" class="form-control form-control-sm"
									id="txtNombres" name="txtNombres" required />
							</div>
						</div>
						<div class="row mt-1">
							<label for="txtApellidos"
								class="col-12 col-sm-3 col-form-label form-control-sm">Apellidos:</label>
							<div class="col-12 col-sm-5">
								<input type="text" class="form-control form-control-sm"
									id="txtApellidos" name="txtApellidos" required />
							</div>
						</div>
						<div class="row mt-1">
							<label for="txtApellidos"
								class="col-12 col-sm-3 col-form-label form-control-sm">Edad:</label>
							<div class="col-12 col-sm-5">
								<input type="text" class="form-control form-control-sm"
									id="txtEdad" id="txtEdad">
							</div>
						</div>

					</div>
					<div class="modal-footer p-2">
						<button type="button" id="btnGuardarCliente"
							class="btn w-25 btn-primary">Guardar</button>
						<button type="button" class="btn w-25 btn-secondary"
							data-dismiss="modal">Cancelar</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<!-- jquery-3.5.1 JS-->
	<script type="text/javascript" charset="UTF-8"
		src="libraries/jquery-3.5.1/jquery.min.js"></script>
	<!-- bootstrap-4.2.1 JS  -->
	<script type="text/javascript" charset="UTF-8"
		src="libraries/bootstrap-4.2.1/js/bootstrap.min.js"></script>
	<!-- bootstrap-table-1.15.5 JS -->
	<script type="text/javascript" charset="UTF-8"
		src="libraries/bootstrap-table-1.15.5/dist/bootstrap-table.min.js"></script>
	<script type="text/javascript" charset="UTF-8"
		src="libraries/bootstrap-table-1.15.5/dist/locale/bootstrap-table-es-ES.js"></script>
	<script type="text/javascript" charset="UTF-8"
		src="libraries/bootstrap-table-1.15.5/dist/extensions/group-by-v2/bootstrap-table-group-by.min.js"></script>

	<script>
		$(document).ready(function() {

			$('#btnNuevo').click(function() {
				limpiarFrmCliente();
				$('#modVis').modal('show');
			});

			$('#btnGuardarCliente').click(function() {
				guardarCliente();
				return false;
			});

		});

		var sendData = function(url, postData, success, error, complete) {
			$.ajax({
				url : url,
				type : "POST",
				dataType : "json",
				data : postData,
				success : success,
				error : error,
				complete : complete
			});
		};

		function ajaxClientes(params) {
			var url = window.location.href;
			var postRequest = new Object();
			postRequest.action = 'cargarClientes';
			sendData(url, postRequest, function(result) {
				params.success(result);
				if (result.codigoError != 0) {
					alert(result.mensajeUsuario);
					params.success(null);
				}
			}, function(error) {
				console.log(error);
				params.success(null);
			});

		}

		function guardarCliente() {
			var url = window.location.href;
			var postRequest = new Object();
			postRequest.action = 'guardarCliente';
			postRequest.id = $('#txtId').val();
			postRequest.nombres = $('#txtNombres').val();
			postRequest.apellidos = $('#txtApellidos').val();
			postRequest.edad = $('#txtEdad').val();
			sendData(url, postRequest, function(result) {
				if (result.codigoError == 0) {
					$('#modVis').modal('hide');
					limpiarFrmCliente();
					cargarTablaClientes();		
					
				}
				alert(result.mensajeUsuario);

			}, function(error) {
				console.log(error);
			});
		}
		function limpiarFrmCliente() {
			$('#txtNombres').val('');
			$('#txtApellidos').val('');
			$('#txtEdad').val('');
			$('#txtId').val('');
		}

		function eliminarCliente(id) {
			var url = window.location.href;
			var postRequest = new Object();
			postRequest.action = 'eliminarCliente';
			postRequest.id = id;
			sendData(url, postRequest, function(result) {
				if (result.codigoError == 0) {
					cargarTablaClientes();
				}
				alert(result.mensajeUsuario);

			}, function(error) {
				console.log(error);
			});
		}

		function cargarTablaClientes() {
			var $tablaCliente = $("#tbCliente");
			$tablaCliente.bootstrapTable('removeAll');
			$tablaCliente.bootstrapTable('destroy');
			$tablaCliente.bootstrapTable();
		}

		function operateFormatterEditar(value, row, index) {
			

			return [ '<button type="button" class="btn btn-sm btn-primary update" href="javascript:void(0)" title="Editar">Editar</button>' ]
					.join('');

		}

		function operateFormatterEliminar(value, row, index) {
			return [ '<button type="button" class="btn btn-sm btn-primary update" href="javascript:void(0)" title="Eliminar">Eliminar</button>' ]
					.join('');

		}

		window.operateEventsEditar = {
			'click .update' : function(e, value, row, index) {
				
				$('#txtId').val(row.id);
				$('#txtNombres').val(row.nombres);
				$('#txtApellidos').val(row.apellidos);
				$('#txtEdad').val(row.edad);
				$('#modVis').modal('show');

			}
		}

		window.operateEventsEliminar = {
			'click .update' : function(e, value, row, index) {
				eliminarCliente(row.id);
			}

		}
	</script>
</body>
</html>