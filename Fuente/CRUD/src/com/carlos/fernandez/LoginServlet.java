package com.carlos.fernandez;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.carlos.fernandez.service.ClientService;
import com.carlos.fernandez.util.Configuration;
import com.carlos.fernandez.util.Function;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ClientService clientService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		clientService = new ClientService();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = (HttpSession) request.getSession();
		String username = (String) session.getAttribute("username");
		//session.setAttribute("username",null);
		if (username == null) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(Configuration.JSP_LOGIN);
			dispatcher.forward(request, response);
		} else {
			response.sendRedirect(Configuration.SRV_PRINCIPAL);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		String username = Function.getParameterString(request, "username");
		String password = Function.getParameterString(request, "password");

		try {
			if (username.trim().equals("") || password.trim().equals("")) {
				request.setAttribute("mensajeError", "Ingrese usuario y contraseņa");
				RequestDispatcher rd = request.getRequestDispatcher(Configuration.JSP_LOGIN);
				rd.forward(request, response);
			} else {
				login(request, response, username, password);
			}

		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("mensajeError", "Error al logearse, intete nuevamente");
			RequestDispatcher rd = request.getRequestDispatcher(Configuration.JSP_LOGIN);
			rd.forward(request, response);
		}

	}

	private void login(HttpServletRequest request, HttpServletResponse response, String username, String password)
			throws ServletException, IOException {
		username = username.toLowerCase();
		Map<String, Object> validate = clientService.login(username, password);
		if ((Integer) validate.get("codigoError") == 0) {
			HttpSession session = (HttpSession) request.getSession();
			session.setAttribute("username", username);
			response.sendRedirect(Configuration.SRV_PRINCIPAL);
		} else {
			request.setAttribute("mensajeError", validate.get("mensajeUsuario"));
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(Configuration.JSP_LOGIN);
			dispatcher.forward(request, response);
		}

	}

}
