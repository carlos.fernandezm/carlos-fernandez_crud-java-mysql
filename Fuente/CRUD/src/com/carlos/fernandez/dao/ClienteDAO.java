package com.carlos.fernandez.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.carlos.fernandez.model.Cliente;

public class ClienteDAO {

	public Map<String, Object> guardarCliente(String nombres, String apellidos, Integer edad) {

		Map<String, Object> retorno = new HashMap<>();
		DataSourceManager datasource = DataSourceManager.getInstance();
		Connection conn = null;
		CallableStatement cstmt = null;
		try {
			conn = datasource.getConection();
			cstmt = conn.prepareCall("{call carlos.sp_guardar_cliente(?,?,?)}");
			cstmt.setString(1, nombres);
			cstmt.setString(2, apellidos);
			cstmt.setInt(3, edad);
			Integer ret = cstmt.executeUpdate();
			if (ret > 0) {
				retorno.put("codigoError", 0);
				retorno.put("mensajeUsuario", "Transaccion exitosa");
			} else {
				retorno.put("codigoError", -1);
				retorno.put("mensajeUsuario", "Error al guardar cliente");
			}

		} catch (Exception e) {
			retorno.put("codigoError", -1);
			retorno.put("mensajeUsuario", "Error al guardar cliente");
			e.printStackTrace();
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return retorno;
	}

	public Map<String, Object> editarCliente(Integer id, String nombres, String apellidos, Integer edad) {

		Map<String, Object> retorno = new HashMap<>();
		DataSourceManager datasource = DataSourceManager.getInstance();
		Connection conn = null;
		CallableStatement cstmt = null;
		try {
			conn = datasource.getConection();
			cstmt = conn.prepareCall("{call carlos.sp_editar_cliente(?,?,?,?)}");
			cstmt.setInt(1, id);
			cstmt.setString(2, nombres);
			cstmt.setString(3, apellidos);
			cstmt.setInt(4, edad);
			Integer ret = cstmt.executeUpdate();
			if (ret > 0) {
				retorno.put("codigoError", 0);
				retorno.put("mensajeUsuario", "Transaccion exitosa");
			} else {
				retorno.put("codigoError", -1);
				retorno.put("mensajeUsuario", "Error al editar cliente");
			}

		} catch (Exception e) {
			retorno.put("codigoError", -1);
			retorno.put("mensajeUsuario", "Error al editar cliente");
			e.printStackTrace();
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return retorno;
	}

	public Map<String, Object> eliminarCliente(Integer id) {

		Map<String, Object> retorno = new HashMap<>();
		DataSourceManager datasource = DataSourceManager.getInstance();
		Connection conn = null;
		CallableStatement cstmt = null;
		try {
			conn = datasource.getConection();
			cstmt = conn.prepareCall("{call carlos.sp_eliminar_cliente(?)}");
			cstmt.setInt(1, id);
			Integer ret = cstmt.executeUpdate();
			if (ret > 0) {
				retorno.put("codigoError", 0);
				retorno.put("mensajeUsuario", "Transaccion exitosa");
			} else {
				retorno.put("codigoError", -1);
				retorno.put("mensajeUsuario", "Error al eliminar cliente");
			}

		} catch (Exception e) {
			retorno.put("codigoError", -1);
			retorno.put("mensajeUsuario", "Error al eliminar cliente");
			e.printStackTrace();
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return retorno;
	}

	public Map<String, Object> consultarCliente(int id) {

		Map<String, Object> retorno = new HashMap<>();
		DataSourceManager datasource = DataSourceManager.getInstance();
		CallableStatement cstmt = null;
		Connection conn = null;
		try {
			conn = datasource.getConection();
			cstmt = conn.prepareCall("{? = call carlos.sp_consulta_cliente(?)}");
			cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
			cstmt.setInt(2, id);
			cstmt.executeQuery();
			retorno = readResultSet(cstmt.getResultSet(), retorno);
		} catch (Exception e) {
			retorno.put("codigoError", -1);
			retorno.put("mensajeUsuario", "Error al obtener información");
			e.printStackTrace();
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return retorno;
	}

	public Map<String, Object> consultarClientes() {

		Map<String, Object> retorno = new HashMap<>();
		DataSourceManager datasource = DataSourceManager.getInstance();
		CallableStatement cstmt = null;
		Connection conn = null;
		try {
			conn = datasource.getConection();
			cstmt = conn.prepareCall("{call carlos.sp_consulta_clientes()}");
			cstmt.execute();
			retorno = readResultSet(cstmt.getResultSet(), retorno);
		} catch (Exception e) {
			retorno.put("codigoError", -1);
			retorno.put("mensajeUsuario", "Error al obtener información");
			e.printStackTrace();
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return retorno;
	}

	private Map<String, Object> readResultSet(ResultSet rs, Map<String, Object> retorno) throws Exception {

		int salida = -1;
		List<Cliente> lista = new ArrayList<Cliente>();
		if (rs != null) {
			while (rs.next()) {
				salida = 0;
				Cliente cliente = new Cliente();
				cliente.setId(rs.getInt(1));
				cliente.setNombres(rs.getString(2));
				cliente.setApellidos(rs.getString(3));
				cliente.setEdad(rs.getInt(4));
				lista.add(cliente);
			}
			rs.close();
		}
		String mensajeUsuario = salida == 0 ? "Transaccion exitosa" : "No existen registros";
		retorno.put("codigoError", salida);
		retorno.put("mensajeUsuario", mensajeUsuario);
		retorno.put("rows", lista);
		return retorno;
	}

}
