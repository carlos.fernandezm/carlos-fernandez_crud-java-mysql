package com.carlos.fernandez.dao;

import java.sql.Connection;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import com.carlos.fernandez.util.Propertie;

public class DataSourceManager {
	private static volatile DataSourceManager instance;
	private static Object mutex = new Object();
	private static DataSource dataSource;

	private DataSourceManager() {
		try {
			dataSource = new DataSource();
			PoolProperties prop = new PoolProperties();
			prop.setDriverClassName(Propertie.getString("mysql.className"));
			String a = Propertie.getString("mysql.url");
			prop.setUrl(Propertie.getString("mysql.url"));
			prop.setUsername(Propertie.getString("mysql.user"));
			prop.setPassword(Propertie.getString("mysql.password"));
			prop.setMaxActive(Integer.parseInt(Propertie.getString("mysql.maxActive")));
			prop.setMaxIdle(Integer.parseInt(Propertie.getString("mysql.maxIdle")));
			prop.setMinIdle(Integer.parseInt(Propertie.getString("mysql.minIdle")));
			prop.setInitialSize(Integer.parseInt(Propertie.getString("mysql.initialSize")));
			prop.setMaxWait(Integer.parseInt(Propertie.getString("mysql.maxWait")));
			prop.setRemoveAbandoned(true);
			prop.setRemoveAbandonedTimeout(55);
			prop.setTimeBetweenEvictionRunsMillis(900);
			prop.setTestOnBorrow(true);
			prop.setLogAbandoned(true);

			dataSource.setPoolProperties(prop);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DataSourceManager getInstance() {
		DataSourceManager result = instance;
		if (result == null) {
			synchronized (mutex) {
				result = instance;
				if (result == null)
					instance = result = new DataSourceManager();
			}
		}
		return result;
	}

	
	public Connection getConection() throws Exception {
		return dataSource.getConnection();
	}
}
