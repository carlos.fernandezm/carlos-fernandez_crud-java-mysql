<%@page import="com.carlos.fernandez.util.Configuration"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="robots" content="noindex, nofollow">
<!-- Hojas de Estilos-->
<!-- bootstrap-4.2.1 CSS -->
<link rel="stylesheet" type="text/css"
	href="libraries/bootstrap-4.2.1/css/bootstrap.min.css">
<title>Login</title>
</head>
<body>
	<div class="container">
		<div class="text-center">
			<h1 class="tituloh1">LOGIN</h1>
		</div>
		<div class="m-auto d-flex justify-content-center">
			<form role="form" method="post" id="frmLogin"
				action="<%=Configuration.SRV_LOGIN%>">
				<div class="form-group">
					<input type="text" class="form-control form-control-sm" name="username" placeholder="Usuario" required />
				</div>
				<div class="form-group">
					<input type="password" class="form-control form-control-sm" name="password" placeholder="Password"
						required />
				</div>
				<div class="form-group">
					<input type="submit" value="INGRESAR" class="form-control btn btn-primary"/>
				</div>
			</form>
		</div>
	</div>
	<!-- jquery-3.5.1 JS-->
	<script type="text/javascript" charset="UTF-8"
		src="libraries/jquery-3.5.1/jquery.min.js"></script>
	<!-- bootstrap-4.2.1 JS  -->
	<script type="text/javascript" charset="UTF-8"
		src="libraries/bootstrap-4.2.1/js/bootstrap.min.js"></script>
</body>
</html>