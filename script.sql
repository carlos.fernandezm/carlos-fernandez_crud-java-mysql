create database carlos;

use carlos;

create table cliente(
id INT AUTO_INCREMENT PRIMARY KEY,
nombres VARCHAR(50) NOT NULL,
apellidos VARCHAR(50) NOT NULL,
edad int);

DELIMITER $$

CREATE PROCEDURE sp_consulta_cliente(IN e_id INT)
BEGIN

	select  id,nombres,apellidos,edad from cliente where id=e_id;

END $$
DELIMITER ;

DELIMITER $$

CREATE PROCEDURE sp_consulta_clientes()
BEGIN

	select  id,nombres,apellidos,edad from cliente;

END $$
DELIMITER ;

DELIMITER $$

CREATE PROCEDURE sp_editar_cliente(IN _id INT,
 _nombres varchar(50),
_apellidos varchar(50),
_edad int)
BEGIN

	update cliente set nombres=_nombres, apellidos=_apellidos, edad=_edad WHERE ID=_id;

END $$
DELIMITER ;

DELIMITER $$

CREATE  PROCEDURE sp_eliminar_cliente(IN _id INT)
BEGIN

	DELETE from cliente WHERE ID=_id;

END $$
DELIMITER ;

DELIMITER $$

CREATE PROCEDURE sp_guardar_cliente(IN _nombres varchar(50),
IN _apellidos varchar(50),
IN _edad int)
BEGIN

	insert into  cliente (nombres, apellidos, edad) values(_nombres,_apellidos,_edad);

END $$
DELIMITER ;
